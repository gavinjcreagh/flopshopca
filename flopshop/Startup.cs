﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(flopshop.Startup))]
namespace flopshop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
